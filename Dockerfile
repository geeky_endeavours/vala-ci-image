FROM debian:bookworm

RUN wget https://deb.nodesource.com/setup_18.x -O - | bash - && \
    apt update && apt install -y \
    nodejs \
    valac \
    valadoc \
    meson \
    libglib2.0-dev \
    gobject-introspection \
    pkg-config \
    cmake \
    libgee-0.8-dev \
    ninja-build \
    libsqlite3-dev \
    libgirepository1.0-dev \
    git \
    libvala-0.56-dev

RUN git clone https://github.com/vala-lang/vala-lint && \
    cd vala-lint && meson setup builddir --prefix=/usr && \
    meson compile -C builddir/ && meson install -C builddir/ && \
    cd .. && rm -rf vala-lint;

ENV PATH="$PATH:/usr/bin"
