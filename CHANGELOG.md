## [1.1.1](https://gitlab.com/geeky_endeavours/vala-ci-image/compare/v1.1.0...v1.1.1) (2023-07-29)


### Bug Fixes

* must be build on release ([da56e18](https://gitlab.com/geeky_endeavours/vala-ci-image/commit/da56e18d3d7bc3cae70967a9527ac8e976006879))

# [1.1.0](https://gitlab.com/geeky_endeavours/vala-ci-image/compare/v1.0.2...v1.1.0) (2023-07-29)


### Features

* add vala-lint ([d84ca9a](https://gitlab.com/geeky_endeavours/vala-ci-image/commit/d84ca9ab1bfe072a1913a015f7240ca366fd80bb))

## [1.0.2](https://gitlab.com/geeky_endeavours/vala-ci-image/compare/v1.0.1...v1.0.2) (2023-07-29)


### Bug Fixes

* install valadoc ([f6a16a4](https://gitlab.com/geeky_endeavours/vala-ci-image/commit/f6a16a4202fec6e28a2797fe59da1001f91cb8c8))

## [1.0.1](https://gitlab.com/geeky_endeavours/vala-ci-image/compare/v1.0.0...v1.0.1) (2023-07-28)


### Bug Fixes

* added libgirrepo ([e229d3d](https://gitlab.com/geeky_endeavours/vala-ci-image/commit/e229d3df5282a86d99b94f23f3e5c857b3987bf2))
* rename readme.md to README.md ([6076ad6](https://gitlab.com/geeky_endeavours/vala-ci-image/commit/6076ad6738552e1fd555b541910ae592796ecb57))
