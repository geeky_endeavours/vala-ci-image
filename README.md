# Vala CI Image

This repo exists to create a docker CI image for Vala projects.

Latest image:

`registry.gitlab.com/geeky_endeavours/vala-ci-image:1.1.1`